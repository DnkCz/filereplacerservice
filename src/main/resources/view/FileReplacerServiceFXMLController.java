/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.resources.view;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import main.java.filereplacerservice.helper.BackgroundFileReplacer;
import main.java.filereplacerservice.helper.FileReplacer;

/**
 * FXML Controller class
 *
 * @author Daniel
 */
public class FileReplacerServiceFXMLController implements Initializable {

    @FXML
    private TextField src_txt;
    @FXML
    private TextField dst_txt;
    @FXML
    private ToggleButton auto_check;
    @FXML
    private GridPane myRoot;

    BackgroundFileReplacer backgroundFileReplacer;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    ScheduledFuture<?> replacerHandle;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    /**
     * Starts autochecking source and destination files
     */
    @FXML
    public void autoCheckButtonAction() {
        backgroundFileReplacer = new BackgroundFileReplacer(dst_txt.getText(), src_txt.getText());

        if (auto_check.isSelected()) {
            replacerHandle = scheduler.scheduleWithFixedDelay(backgroundFileReplacer, 0, 5, TimeUnit.DAYS);
        } else {
            //deselected => 
            if (replacerHandle != null) {
                replacerHandle.cancel(true);
            }

        }
    }

    /**
     * Starts one manual check
     */
    @FXML
    public void manualCheckButtonAction() {
        FileReplacer fileReplacer = new FileReplacer();
        fileReplacer.setDestination(dst_txt.getText());
        fileReplacer.setSource(src_txt.getText());
        fileReplacer.replace();
    }

    /**
     * Action called when clicked on "source button". Opens file chooser
     * (browser).
     */
    @FXML
    public void srcButtonAction() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("");
        File f = fileChooser.showOpenDialog(myRoot.getScene().getWindow());
        if (f != null) {
            src_txt.setText(f.getAbsolutePath());
        }
    }

    /**
     * Action called when clicked on "destination button". Opens file chooser
     * (browser).
     */
    @FXML
    public void dstButtonAction() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("");
        File f = fileChooser.showOpenDialog(myRoot.getScene().getWindow());
        if (f != null) {
            dst_txt.setText(f.getAbsolutePath());
        }
    }

}
