/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.filereplacerservice.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author Daniel
 */
public final class FileReplacerService extends Application {

    // ISO-639 Language Code 
    private static String language;
    // ISO-3166 Country Codes 
    private static String country;

    //command line parameters
    private static final String COMMAND_LINE_PARAMETER_SRC = "src";
    private static final String COMMAND_LINE_PARAMETER_DST = "dst";
    private static final String COMMAND_LINE_PARAMETER_LANGUAGE = "lang";
    private static final String COMMAND_LINE_PARAMETER_COUNTRY = "country";

    //
    private static final String SRC_LOCALIZATION = "src/bundles/localization";

    private static String source;
    private static String destination;
    private static Locale locale;

    private static Properties properties;
    private static ResourceBundle GUIBundle;

    private static final String PROPERTIES_RELATIVE_PATH = "src/settings.properties";

    /**
     * Determines time in miliseconds when Thread is put to sleep and stop
     * working
     */
    public static final long SLEEP_INTERVAL = 60000;

    /**
     * @param args the command line arguments There are three ways how to set
     * language and country. First it gets parameters from command line. If no
     * parameters passed it takes parameters from settings.properties file which
     * might containt last configuration. If no last configuration in
     * settings.properties file it tryies to determine language from 'os'
     * property variable.
     *
     * crutial parameter is language. If no exact match found aka.. we pass
     * en_GB, though we have en_US locale (where 'en' is language and 'GB' and
     * 'US' are countries) then en_US locale is used.
     *
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        try {// default configuration
            properties = new Properties();

            try (FileInputStream in = new FileInputStream(new File(PROPERTIES_RELATIVE_PATH))) {
                properties.load(in);
            } catch (IOException ex) {
                Logger.getLogger(FileReplacerService.class.getName()).log(Level.SEVERE, null, ex);
            }

            Options options = new Options();
            options.addOption(COMMAND_LINE_PARAMETER_SRC, true, "Source (our) file path");
            options.addOption(COMMAND_LINE_PARAMETER_DST, true, "Destination (to be replaced) file path");
            options.addOption(COMMAND_LINE_PARAMETER_LANGUAGE, true, "Language (en,cs...) according to ISO-639 Language Codes ");
            options.addOption(COMMAND_LINE_PARAMETER_COUNTRY, true, "Country (US,CZ...) according to ISO-3166 Country Codes  ");

            DefaultParser defaultParser = new DefaultParser();
            CommandLine cmd = defaultParser.parse(options, args);

            if (cmd.hasOption(COMMAND_LINE_PARAMETER_SRC)) {
                FileReplacerService.source = cmd.getOptionValue(COMMAND_LINE_PARAMETER_SRC);

            }

            if (cmd.hasOption(COMMAND_LINE_PARAMETER_SRC)) {
                FileReplacerService.destination = cmd.getOptionValue(COMMAND_LINE_PARAMETER_DST);
            }

            if (cmd.hasOption(COMMAND_LINE_PARAMETER_LANGUAGE)) {
                FileReplacerService.language = cmd.getOptionValue(COMMAND_LINE_PARAMETER_LANGUAGE);
            } else if (properties.containsKey("lang") && !properties.getProperty("lang").isEmpty()) {
                // read last used language if any setted
                FileReplacerService.language = properties.getProperty("lang");
                // if country set as well
                if (properties.containsKey("country") && !properties.getProperty("country").isEmpty()) {
                    FileReplacerService.country = properties.getProperty("country");
                }
            } else {
                // try to get language from system
                String systemLang = System.getProperty("user.language");
                if (systemLang != null && (systemLang.length() > 0)) {
                    FileReplacerService.language = systemLang;
                }
            }

            if (cmd.hasOption(COMMAND_LINE_PARAMETER_COUNTRY)) {
                FileReplacerService.country = cmd.getOptionValue(COMMAND_LINE_PARAMETER_COUNTRY);
            } else {
                // try to get country from system
                String systemCountry = System.getProperty("user.country");
                if (systemCountry != null && (systemCountry.length() > 0)) {
                    FileReplacerService.country = systemCountry;
                }
            }

            // check if exists exact locale, or same language
            File[] srcDirectoryFiles = new File(SRC_LOCALIZATION).listFiles();
            String tmpCountry = "";
            boolean fullMatch = false;
            for (File f : srcDirectoryFiles) {
                if (f.isFile()) {
                    if (f.getName().contains("_" + language + "_" + country)) {
                        fullMatch = true;
                        break;
                    }

                    // list files if any language
                    if (f.getName().contains("_" + language)) {
                        // we take last part which is country code and save it to temporary variable
                        try {

                            tmpCountry = f.getName().split("\\.")[0]; // abc.properties -> [gui_cs_CZ.properties] [gui_cs_CZ] to next part
                            tmpCountry = tmpCountry.split("_")[2];
                        } catch (ArrayIndexOutOfBoundsException e) {
                            tmpCountry = "";
                        }
                    }
                }
            }

            if (fullMatch) {
                locale = new Locale(language, country);
            } else if (tmpCountry.length() > 0) {
                country = tmpCountry;
                locale = new Locale(language, country);
            } else {
                if (properties.containsKey(country)) {
                    country = properties.getProperty(country);
                }

                if (properties.containsKey(language)) {
                    language = properties.getProperty(language);
                }

                if (country.length() > 0 && language.length() > 0) {
                    locale = new Locale(language, country);
                }
            }

            if (locale == null) {
                System.exit(1); //locale not setted
            }

            properties.setProperty("country", locale.getCountry());
            properties.setProperty("lang", locale.getLanguage());

            // write changes to properties file
            try (FileOutputStream out = new FileOutputStream(PROPERTIES_RELATIVE_PATH)) {
                properties.store(out, "");
            } catch (IOException ex) {
                Logger.getLogger(FileReplacerService.class.getName()).log(Level.SEVERE, null, ex);
            }

            // gui bundle load
            GUIBundle = ResourceBundle.getBundle("bundles.localization.gui", locale);

            // set current lang, country and locale
            launch(args);
            /* @Test
            String source="c:\\Users\\Daniel\\Pictures\\wallpaper_wolf.jpg";
            String destination="c:\\Users\\Daniel\\Downloads\\wallpaper_wolf.jpg";
            FileReplacer.replace(source, destination);
             */
        } catch (ParseException ex) {
            Logger.getLogger(FileReplacerService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/main/resources/view/FileReplacerFXML.fxml"));
        loader.setResources(GUIBundle);
        Parent root = loader.load();

        primaryStage.setTitle(GUIBundle.getString("title"));

        Scene scene = new Scene(root, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

}
