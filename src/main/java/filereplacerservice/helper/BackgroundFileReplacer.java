/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.filereplacerservice.helper;

/**
 *
 * @author Daniel
 */
public class BackgroundFileReplacer implements Runnable {
    FileReplacer fileReplacer;
   
    
    /**
     * Constructor for BackgroundFileReplacer class
     * @param destination
     * @param source 
     */
    public BackgroundFileReplacer(String destination, String source) {
        this.fileReplacer = new FileReplacer();
        fileReplacer.setDestination(destination);
        fileReplacer.setSource(source);
    }

    @Override
    public void run() {
        fileReplacer.replace();
    }

   
}
