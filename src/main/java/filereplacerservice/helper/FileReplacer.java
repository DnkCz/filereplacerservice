/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.filereplacerservice.helper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import main.java.oshelper.OSHelper;

/**
 *
 * @author Daniel
 */
public class FileReplacer {

    String source;
    String destination;

    /**
     * Getter for source file path
     *
     * @return source
     */
    public String getSource() {
        return source;
    }

    /**
     * Setter for source file path
     *
     * @param source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * Getter for destination parameter
     *
     * @return
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Setter for destination file path
     *
     * @param destination
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     * source(our file) and destination(file to be replaced)
     */
    public void replace() {

        if (this.destination == null || this.source == null) {
            return;
        }

        OSHelper oSHelper = new OSHelper();
        if (oSHelper.isWindows()) {
            System.out.println("is Windows");
            System.out.println(source);
            source = source.replace("\\", "\\\\"); // replace \ as \\
            System.out.println(source);
        }

        File sourceFile = new File(source);
        File destinationFile = new File(destination);

        if (!sourceFile.exists()) {
            System.out.println("Source file does NOT exists");
            return;
        }

        if (!destinationFile.exists()) {
            System.out.println("Destination file does NOT exists");
            return;
        }

        if (sourceFile.length() == destinationFile.length()) {
            System.out.println("Files are of the same size");
            return;
        }
        System.out.println("Replacing...");

        try {
            Files.copy(sourceFile.toPath(), destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            System.out.println("Replaced");
        } catch (IOException ex) {
            // Logger.getLogger(FileReplacer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Replaces source with with the destination file
     *
     * @param source
     * @param destination
     */
    public void replace(String source, String destination) {
        this.setDestination(destination);
        this.setSource(source);
        this.replace();
    }
}
