/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.oshelper;

import java.util.Locale;

/**
 *
 * @author Daniel
 */
/**
 * Used to determine operating system
 */
public class OSHelper {

    String osname;

    /**
     * Constructor
     */
    public OSHelper() {
        this.osname = System.getProperty("os.name").toLowerCase(Locale.US);
    }

/**
 * Check wheter platform is windows.
 * @return 
 */
    public boolean isWindows() {
        return this.osname.contains("win");
    }

    /**
     * Checks wheter platform is *nix
     * @return 
     */
    public boolean isUnix() {
        return this.osname.contains("nix");
    }

}
